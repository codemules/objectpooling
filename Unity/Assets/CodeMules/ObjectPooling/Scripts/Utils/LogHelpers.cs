//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of 
//  this software and associated documentation files (the "Software"), to deal in 
//  the Software without restriction, including without limitation the rights to use, 
//  copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//  Software, and to permit persons to whom the Software is furnished to do so, 
//  subject to the following conditions:
//  
//  The above copyright notice and this permission notice shall be included in all 
//  copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using UnityEngine;

using System;
using System.Reflection;

namespace CodeMules.Utils {
	/// <summary>
	/// Helpers methods to simplify debug logging and to make logging
	/// conditional
	/// </summary>
	public static class LogHelpers {
		/// <summary>
		/// Log a diagnostic message
		/// </summary>
		/// <param name="format">format string</param>
		/// <param name="args">format string arguments</param>
		[System.Diagnostics.Conditional("DEBUG")]
		[System.Diagnostics.Conditional("DEVELOPMENT_BUILD")]
		public static void Log(string format,params object [] args) {
			Debug.LogFormat(format,args);
		}

		/// <summary>
		/// Log a warning message
		/// </summary>
		/// <param name="format">format string</param>
		/// <param name="args">format string arguments</param>
		[System.Diagnostics.Conditional("DEBUG")]
		[System.Diagnostics.Conditional("DEVELOPMENT_BUILD")]
		public static void LogWarning(string format,params object [] args) {
			Debug.LogWarningFormat(format,args);
		}

		/// <summary>
		/// Log an error message
		/// </summary>
		/// <param name="format">format string</param>
		/// <param name="args">format string arguments</param>
		[System.Diagnostics.Conditional("DEBUG")]
		[System.Diagnostics.Conditional("DEVELOPMENT_BUILD")]
		public static void LogError(string format,params object [] args) {
			Debug.LogErrorFormat(format,args);
		}

		/// <summary>
		/// Log an error message
		/// </summary>
		/// <param name="ex">The exception to log</param>
		[System.Diagnostics.Conditional("DEBUG")]
		[System.Diagnostics.Conditional("DEVELOPMENT_BUILD")]
		public static void LogError(Exception ex) {
			ex = GetBestException(ex);
			Debug.LogErrorFormat("{0}: {1}",ex.GetType().Name,ex.Message);
		}

		/// <summary>
		/// Log an error message
		/// </summary>
		/// <param name="ex">The exception to log</param>
		/// <param name="message">Error message related to the exception</param>
		[System.Diagnostics.Conditional("DEBUG")]
		[System.Diagnostics.Conditional("DEVELOPMENT_BUILD")]
		public static void LogError(Exception ex,string message) {
			ex = GetBestException(ex);
			Debug.LogErrorFormat("{0} => {1}: {2}",message,ex.GetType().Name,ex.Message);
		}

		/// <summary>
		/// Search an exception hierarchy for anything that is NOT
		/// an <see cref="TargetInvocationException"/>
		/// </summary>
		/// <returns>The first exception that is not an instance of <see cref="TargetInvocationException"/></returns>
		/// <param name="ex">The exception/param>
		public static Exception GetBestException(Exception ex) {
			Exception result = ex ;

			TargetInvocationException tie ;

			do {
				tie = result as TargetInvocationException ;

				if (tie != null) {
					//	----------------------------------------------------------------
					//	We have to use the Tie as the best exception because we do not 
					//	have an inner exception
					//					------------------------
					//	Never seen this but we have to handle it
					//	----------------------------------------------------------------
					if (tie.InnerException == null)
						break ;

					// use the inner exception of the TargetInvocationException
					result = tie.InnerException ;
				}
			} while(tie != null) ;

			return result ;
		}
	}
}
