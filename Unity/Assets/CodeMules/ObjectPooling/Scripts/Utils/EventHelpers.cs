﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of 
//  this software and associated documentation files (the "Software"), to deal in 
//  the Software without restriction, including without limitation the rights to use, 
//  copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//  Software, and to permit persons to whom the Software is furnished to do so, 
//  subject to the following conditions:
//  
//  The above copyright notice and this permission notice shall be included in all 
//  copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using System;

namespace CodeMules.Utils {
	public static class EventHelpers  {
		/// <summary>
		/// Publish an event to listeners
		/// </summary>
		/// <param name="theEvent">The listeners</param>
		/// <param name="eventArgs">The arguments for the event</param>
		/// <remarks>
		/// Using this method works around potential problems when an event listener
		/// throws an exception by catching and ignoring the exception and continuing
		/// the event publication to other listeners. By default, .NET terminates
		/// the event publication if an event listener throws an exception.
		/// <para>
		/// This method can be used for any form of delegate; just make sure that
		/// <paramref name="eventArgs"/> matches the target in both order and type
		/// of the arguments.
		/// </para>
		/// </remarks>
		public static void Publish(Delegate theEvent,params object[] eventArgs) {
			if (theEvent != null) {
				Delegate [] listeners ;

				lock(theEvent) {
					// get the listeners
					listeners = theEvent.GetInvocationList() ;
				}

				foreach(Delegate listener in listeners) {
					try {
						listener.DynamicInvoke(eventArgs) ;
					} catch(Exception ex) {
						LogHelpers.LogError(ex,"Event Listener Exception");
					}
				}
			}
		}
	}
}
