﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of 
//  this software and associated documentation files (the "Software"), to deal in 
//  the Software without restriction, including without limitation the rights to use, 
//  copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//  Software, and to permit persons to whom the Software is furnished to do so, 
//  subject to the following conditions:
//  
//  The above copyright notice and this permission notice shall be included in all 
//  copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using System ;
using System.Diagnostics;

namespace CodeMules.Utils {
	/// <summary>
	/// Helper methods for validating parameters
	/// </summary>
	public static class Validation {
		private const string NoParamName = "(no param name)" ;

		#region CheckNotNull
		/// <summary>
		/// Validate that a parameter is NOT <c>null</c>
		/// </summary>
		/// <param name="obj">The parameter to validate</param>
		/// <param name="name">
		/// The optional name of the parameter being validated
		/// </param>
		/// <exception cref='ArgumentNullException'>
		/// thrown when an argument passed to a method is invalid because it is <see langword="null" /> .
		/// </exception>
		/// <remarks>>
		/// This is using the <see cref="object.ReferenceEquals"/> function to ignore virtual Equals operators and/or 
		/// static operators that might not perform a reference comparison.
		/// </remarks>
		[Conditional("DEBUG")]
		[Conditional("DEVELOPMENT_BUILD")]
		public static void CheckNotNull(object obj, string name = null) {
			if (ReferenceEquals(obj, null)) {
				Exception ex = new ArgumentNullException(name ?? NoParamName);
				LogHelpers.LogError(ex);
				throw ex ;
			}
		}
		#endregion

		#region CheckString
		/// <summary>
		/// Checks a string to validate that it is NOT null and NOT empty
		/// </summary>
		/// <param name='str'>
		/// The string to validate
		/// </param>
		/// <param name="name">
		/// The optional name of the parameter being validated
		/// </param>
		[Conditional("DEBUG")]
		[Conditional("DEVELOPMENT_BUILD")]
		public static void CheckString(string str, string name = null) {
			CheckNotNull(str, name);
			CheckNotEmpty(str, name);
		}

		/// <summary>
		/// Check that a string is NOT empty (effectively 0 length)
		/// </summary>
		/// <param name='str'>
		/// The string to validate
		/// </param>
		/// <param name='name'>
		/// The optional name of the parameter being validated
		/// </param>
		/// <exception cref='ArgumentException'>
		/// If the trimmed length of the string is 0
		/// </exception>
		/// <remarks>
		/// It is ok if <paramref name="str"/> is <see langword="null"/>
		/// </remarks>
		[Conditional("DEBUG")]
		[Conditional("DEVELOPMENT_BUILD")]
		public static void CheckNotEmpty(string str, string name = null) {
			// its ok if str == null
			if (str != null) {
				if (str.Trim().Length == 0) {
					Exception ex = new ArgumentException("String cannot be empty", name ?? NoParamName);
					LogHelpers.LogError(ex);
					throw ex ;

				}
			}
		}
		#endregion

		#region CheckRange Int32
		/// <summary>
		/// Valdiate that an <see cref="int"/> value is &gt; 0
		/// </summary>
		/// <param name="value">The integer to validate</param>
		/// <param name='name'>
		/// The optional name of the parameter being validated
		/// </param>
		/// <seealso cref="CheckNotNegative"/>
		[Conditional("DEBUG")]
		[Conditional("DEVELOPMENT_BUILD")]
		public static void CheckPositive(int value, string name = null) {
			CheckRangeInclusive(value, 1, int.MaxValue, name);
		}

		/// <summary>
		/// Valdiate that an <see cref="int"/> value is &gt;= 0
		/// </summary>
		/// <param name="value">The integer to validate</param>
		/// <param name='name'>
		/// The optional name of the parameter being validated
		/// </param>
		/// <seealso cref="CheckPositive"/>
		[Conditional("DEBUG")]
		[Conditional("DEVELOPMENT_BUILD")]
		public static void CheckNotNegative(int value, string name = null) {
			CheckRangeInclusive(value, 0, int.MaxValue, name);
		}

		/// <summary>
		/// Valdiate that an <see cref="int"/> value is within a range
		/// </summary>
		/// <param name="value">The integer to validate</param>
		/// <param name="min">optional inclusive lower bound</param>
		/// <param name="max">optional inclusive upper bound</param>
		/// <param name='name'>
		/// The optional name of the parameter being validated
		/// </param>
		/// <seealso cref="CheckRangeExclusive"/>
		[Conditional("DEBUG")]
		[Conditional("DEVELOPMENT_BUILD")]
		public static void CheckRangeInclusive(int value, int? min, int? max, string name = null) {
			Exception ex = null ;

			if (min.HasValue && (value < min)) {
				ex = new ArgumentOutOfRangeException(
					name ?? NoParamName,
					string.Format(
						"Value must be >= {0}",
						min)
				);
			}

			if (max.HasValue && (value > max)) {
				ex = new ArgumentOutOfRangeException(
					name ?? NoParamName,
					string.Format(
						"Value must be <= {0}",
						max)
				);
			}

			if (ex != null) {
				LogHelpers.LogError(ex);
				throw ex ;
			}

		}

		/// <summary>
		/// Valdiate that an <see cref="int"/> value is within a range
		/// </summary>
		/// <param name="value">The integer to validate</param>
		/// <param name="min">optional exclusive lower bound</param>
		/// <param name="max">optional exclusive upper bound</param>
		/// <param name='name'>
		/// The optional name of the parameter being validated
		/// </param>
		/// <seealso cref="CheckRangeInclusive"/>
		[Conditional("DEBUG")]
		[Conditional("DEVELOPMENT_BUILD")]
		public static void CheckRangeExclusive(int value, int? min, int? max, string name = null) {
			Exception ex = null ;

			if (min.HasValue && (value <= min)) {
				ex = new ArgumentOutOfRangeException(
					name ?? NoParamName,
					string.Format(
						"Value must be > {0}",
						min)
				);
			}

			if (max.HasValue && (value >= max)) {
				ex = new ArgumentOutOfRangeException(
					name ?? NoParamName,
					string.Format(
						"Value must be < {0}",
						max)
				);
			}

			if (ex != null) {
				LogHelpers.LogError(ex);
				throw ex ;
			}
		}
		#endregion CheckRange Int32	
	}
}
