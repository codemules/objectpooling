//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of 
//	this software and associated documentation files (the "Software"), to deal in 
//	the Software without restriction, including without limitation the rights to use, 
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//	Software, and to permit persons to whom the Software is furnished to do so, 
//	subject to the following conditions:
//  
//	The above copyright notice and this permission notice shall be included in all 
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using System ;
using CodeMules.Utils;

namespace CodeMules.ObjectPooling {
	/// <summary>
	/// Class that wraps an item for use in an <see cref="IObjectPool{T}"/> and
	/// allows consumers to use a <c>using(item = pool.Acquire())</c> paradigm to
	/// simplify pool resource management by returning the resource back to the 
	/// pool when the wrapper is 'disposed'
	/// </summary>
	/// <remarks>
	/// If <typeparamref name="T"/> implements <see cref="IDisposable"/>, then
	/// it will be disposed when the it is no longer needed (pool closes or normal GC)
	/// </remarks>
	public class PoolWrapper<T> : IDisposable where T : class, new() {
		#region Fields
		/// <summary>
		/// The pool that manages this resource; where we release
		/// </summary>
		private IObjectPool<PoolWrapper<T>> Pool { get ; set ; }
		/// <summary>
		/// The instance being wrapped
		/// </summary>
		private T resource ;
		/// <summary>
		/// Determine if the wrapper has been disposed
		/// </summary>
		private bool isDisposed ;
		#endregion

		#region Static Operators
		/// <summary>
		/// Convert the wrapper to the underlying <see cref="Instance"/>
		/// </summary>
		/// <param name="wrapper">The pool wrapper</param>
		/// <exception cref="ObjectDisposedException">If this wrapper has been disposed</exception>
		public static implicit operator T(PoolWrapper<T> wrapper) {
			return wrapper.Resource;
		}
		#endregion

		#region Constructor(s)
		/// <summary>
		/// Initializes a new instance of the <see cref="CodeMules.ObjectPooling.PoolWrapper{T}"/> class.
		/// </summary>
		/// <param name="pool">The pool that manages this resource</param>
		private PoolWrapper(IObjectPool<PoolWrapper<T>> pool) {
			Validation.CheckNotNull(pool);

			Pool = pool;
			resource = new T() ;
		}
		#endregion

		#region IDisposable implementation
		/// <summary>
		/// Release/Dispose wrapper and instance
		/// </summary>
		/// <param name="isDirectDispose"><c>true</c> if <see cref="Dispose()"/> was called directly</param>
		/// <remarks>
		/// If <see cref="Pool"/> is still open/active then we release back so the resource can be re-used, 
		/// otherwise we do the actual dispose of <see cref="instance"/> and this wrapper
		/// </remarks>
		protected virtual void Dispose(bool isDirectDispose) {
			if (isDirectDispose) {
				if (Pool.IsOpen) {
					Pool.Release(this);
				} else {
					isDisposed = true ;

					IDisposable disposable = resource as IDisposable;

					if (disposable != null) {
						disposable.Dispose();
					}

					resource = null ;
					Pool = null ;
					GC.SuppressFinalize(this) ;
				}
			}
		}

		/// <summary>
		/// Release all resources used by the <see cref="CodeMules.ObjectPooling.PoolWrapper{T}"/> object.
		/// </summary>
		/// <remarks>Call <see cref="Dispose()"/> when you are finished using the <see cref="ObjectPooling.PoolWrapper{T}"/>.
		/// The <see cref="Dispose()"/> method leaves the <see cref="ObjectPooling.PoolWrapper{T}"/> in an unusable
		/// state. After calling <see cref="Dispose()"/>, you must release all references to the
		/// <see cref="ObjectPooling.PoolWrapper{T}"/> so the garbage collector can reclaim the memory that the
		/// <see cref="ObjectPooling.PoolWrapper{T}"/> was occupying.</remarks>
		public virtual void Dispose() {
			Dispose(true);
			// Note: GC.SuppressFinalize(this) should not be called here. See Dispose(bool)
		}

		/// <summary>
		/// Releases unmanaged resources and performs other cleanup operations before the
		/// <see cref="CodeMules.ObjectPooling.PoolWrapper{T}"/> is reclaimed by garbage collection.
		/// </summary>
		~PoolWrapper() {
			Dispose(false);
		}
		#endregion

		#region Properties
		/// <summary>
		/// Get the underlying resource 
		/// </summary>
		/// <value>The resource</value>
		/// <exception cref="ObjectDisposedException">If this wrapper has been disposed</exception>
		public T Resource { 
			get {
				if (isDisposed) {
					throw new ObjectDisposedException("PoolWrapper and underlying resource have been disposed");
				}

				return resource ;
			}
		}
		#endregion

		#region Pool Helpers
		/// <summary>
		/// Create a pool for managing instances
		/// </summary>
		/// <returns>The object pool</returns>
		/// <param name="poolSize">The size of the pool</param>
		/// <param name="preAllocate">The number of instances to pre-allocate for immediate use</param>
		/// <param name="resetter">(optional) method that "resets" an resource when it is released to the pool</param>
		public static IObjectPool<PoolWrapper<T>> CreatePool(
							int poolSize, 
							int preAllocate = 0,
							Func<PoolWrapper<T>,PoolWrapper<T>> resetter = null
							) 
		{
			return new ObjectPool<PoolWrapper<T>>(
				(pool) => new PoolWrapper<T>(pool),
				resetter,
				poolSize,
				preAllocate
				);
		}
		#endregion
	}
}
