//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of 
//	this software and associated documentation files (the "Software"), to deal in 
//	the Software without restriction, including without limitation the rights to use, 
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//	Software, and to permit persons to whom the Software is furnished to do so, 
//	subject to the following conditions:
//  
//	The above copyright notice and this permission notice shall be included in all 
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using UnityEngine;

using System ;
using CodeMules.Utils;

namespace CodeMules.ObjectPooling {
	/// <summary>
	/// A <see cref="MonoBehaviour"/> attached to resources so we know which pool
	/// the resource belongs to
	/// </summary>
	public class GameObjectPoolId : MonoBehaviour {
		[NonSerialized]
		private string poolId ;

		/// <summary>
		/// Get/Set the pool identifier that manages this resource
		/// </summary>
		/// <value>The pool identifier</value>
		public string PoolId {
			get {
				return poolId ;
			}

			internal set {
				if (poolId != null) {
					LogHelpers.LogWarning(
						"Attempt to re-assign {0}.PoolId ('{1}' => '{2}'). The value of PoolId should be considered write-once.",
						typeof(GameObjectPoolId).Name,
						poolId,
						value
					);

					return ;
				}

				poolId = value ;
			}
		}
	}
}
