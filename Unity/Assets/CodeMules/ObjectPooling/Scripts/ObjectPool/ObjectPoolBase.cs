//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of 
//	this software and associated documentation files (the "Software"), to deal in 
//	the Software without restriction, including without limitation the rights to use, 
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//	Software, and to permit persons to whom the Software is furnished to do so, 
//	subject to the following conditions:
//  
//	The above copyright notice and this permission notice shall be included in all 
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using System;
using CodeMules.Utils ;

namespace CodeMules.ObjectPooling {
	/// <summary>
	/// Base re-usable functionality for things implementing <see cref="IObjectPool{T}"/>
	/// </summary>
	public abstract class ObjectPoolBase<T> : IObjectPool<T> where T : class {
		#region Fields
		/// <summary>
		/// indicates whether the instance has been disposed
		/// </summary>
		private bool isDisposed ;

		/// <summary>
		/// Listeners for pool observations
		/// </summary>
		/// <remarks>
		/// See <see cref="Observe"/>
		/// </remarks>
		private Action<IObjectPoolBase,PoolObservationType,object> poolObservers ;
		#endregion

		#region Constructor(s)
		/// <summary>
		/// Initializes a new instance of the <see cref="ObjectPooling.ObjectPoolBase{T}"/> class.
		/// </summary>
		/// <param name="id">The unique pool identifier</param>
		/// <param name="size">The target size of the pool</param>
		protected ObjectPoolBase(string id,int size) {
			Validation.CheckString(id);
			Validation.CheckPositive(size);

			Id = id ;
			Size = size ;
		}
		#endregion

		#region IObjectPool<T> implementation
		#region Methods
		/// <summary>
		/// Acquire a resource from the pool
		/// </summary>
		public abstract T Acquire() ;

		/// <summary>
		/// Release a resource to the pool
		/// </summary>
		/// <param name="resource">The resource to be released to the pool</param>
		public abstract void Release(T resource) ;

		/// <summary>
		/// Release a resource to the pool
		/// </summary>
		/// <param name="resource">The resource to be released to the pool</param>
		public virtual void ReleaseSafe(ref T resource) {
			// release to the pool
			Release(resource);
			// clear the callers reference
			resource = null;
		}

		/// <summary>
		/// If the pool is tracking <paramref name="resource"/> in any way, the
		/// resource should be forgotten
		/// </summary>
		/// <param name="resource">The resource that should be forgotten</param>
		/// <remarks>
		/// This method is designed to allow an external entity that has acquired a resource
		/// to take control of it, and let the pool know that it should not be tracked in any way
		/// <para>
		/// It is implied that <paramref name="resource"/> has been acquired as the caller
		/// should not have a reference to it otherwise
		/// </para>
		/// </remarks>
		/// <exception cref="ObjectDisposedException">If the method is called after the pool has been disposed</exception>
		public virtual void Forget(T resource) {
			CheckClosed() ;

			Validation.CheckNotNull(resource) ;

			//	-------------------------------------------------------------------------
			//	By default this is a NOP, but implementations that actually track
			//	allocated resources should make an observation such as the following:
			// 		PublishObservation(PoolObservationType.Forgotten, resource); 
			//			-- OR when the resource is not being tracked --
			//		PublishObservation(PoolObservationType.ForgetUnknown, resource);
			//	-------------------------------------------------------------------------
		}
		#endregion

		#region Properties 
		/// <summary>
		/// A name for the <c>ObjectPool{T}</c>
		/// </summary>
		/// <value>The name of the pool</value>
		/// <remarks>
		/// Useful for debugging
		/// </remarks>
		public string Id { get ; private set ; }

		/// <summary>
		/// Get the target size of the pool
		/// </summary>
		/// <value>The target size of the pool</value>
		public int Size { get ; private set ; }

		/// <summary>
		/// Determine if the <see cref="IObjectPool{T}"/> is open
		/// </summary>
		/// <value>
		/// <c>true</c> if resources can be acquired/released, <c>false</c> otherwise
		/// </value>
		public bool IsOpen {
			get {
				return !IsClosed;
			}
		}

		/// <summary>
		/// Determine if the <see cref="IObjectPool{T}"/> is closed
		/// </summary>
		/// <value>
		/// <c>true</c> if the pool is closed and no resources are available, 
		/// <c>false</c> otherwise
		/// </value>
		public bool IsClosed {
			get {
				return isDisposed ;
			}
		}
		#endregion

		#region Events
		/// <summary>
		/// Subscribe/Unsubscribe to pool observations
		/// </summary>
		/// <value>The observer</value>
		public event Action<IObjectPoolBase,PoolObservationType,object> Observe { 
			add {
				poolObservers += value ;
			}

			remove {
				poolObservers -= value ;	
			}
		}
		#endregion
		#endregion

		#region Implementation
		/// <summary>
		/// Takes the measurement.
		/// </summary>
		/// <param name="observation">The type of the observation</param>
		/// <param name="resource">The resource involved in the operation</param>
		[System.Diagnostics.Conditional("DEBUG")]
		[System.Diagnostics.Conditional("DEVELOPMENT_BUILD")]
		[System.Diagnostics.Conditional("POOL_MEASUREMENTS")]
		protected void PublishObservation(PoolObservationType observation,object resource) {
			if (poolObservers != null) {
				EventHelpers.Publish(poolObservers,this,observation,resource);
			}
		}

		/// <summary>
		/// Determine if the pool is closed, and if so, thrown an exception
		/// </summary>
		/// <exception name="ObjectDisposedException">Thrown if the pool is closed</exception>
		protected void CheckClosed() {
			if (IsClosed) {
				// DisposedError
				throw new ObjectDisposedException("Cannot use Object Pool that has been closed/disposed") ;
			}
		}
		#endregion

		#region IDisposable Implementation
		/// <summary>
		/// Required to be implemented by derived classes to dispose of pool resources
		/// </summary>
		protected abstract void DisposeResources() ;

		/// <summary>
		/// Dispose the pool, and any resources that implement <see cref="IDisposable"/>
		/// </summary>
		/// <param name="isDirectDispose">
		/// <c>true</c> if <see cref="Dispose()"/> was called directly, 
		/// <c>false</c> if we are being called from the finalizer
		/// </param>
		protected void Dispose(bool isDirectDispose) {
			if (isDirectDispose) {
				// Dispose the resources in the pool
				DisposeResources();

				// no more observations
				poolObservers = null ;
			}
		}

		/// <summary>
		/// Releases all resource used by the <see cref="ObjectPooling.ObjectPoolBase{T}"/> object.
		/// </summary>
		/// <remarks>Call <see cref="Dispose()"/> when you are finished using the <see cref="ObjectPooling.ObjectPoolBase{T}"/>.
		/// The <see cref="Dispose()"/> method leaves the <see cref="ObjectPooling.ObjectPoolBase{T}"/> in an unusable
		/// state. After calling <see cref="Dispose()"/>, you must release all references to the
		/// <see cref="ObjectPooling.ObjectPoolBase{T}"/> so the garbage collector can reclaim the memory that the
		/// <see cref="ObjectPooling.ObjectPoolBase{T}"/> was occupying.</remarks>
		public void Dispose() {
			if (isDisposed)
				return ;

			isDisposed = true ;

			Dispose(true) ;

			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Releases unmanaged resources and performs other cleanup operations before the
		/// <see cref="ObjectPooling.ObjectPoolBase{T}"/> is reclaimed by garbage collection.
		/// </summary>
		~ObjectPoolBase() {
			Dispose(false);
		}
		#endregion
	}
}
