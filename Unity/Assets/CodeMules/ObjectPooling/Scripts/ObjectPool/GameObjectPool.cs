//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of 
//	this software and associated documentation files (the "Software"), to deal in 
//	the Software without restriction, including without limitation the rights to use, 
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//	Software, and to permit persons to whom the Software is furnished to do so, 
//	subject to the following conditions:
//  
//	The above copyright notice and this permission notice shall be included in all 
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using UnityEngine;

using System ;

using CodeMules.Utils;

namespace CodeMules.ObjectPooling {
	/// <summary>
	/// Object Pool for Unity Game Object's instantiated from a prefab
	/// </summary>
	public class GameObjectPool : ObjectPoolBase<GameObject> {
		#region Fields
		/// <summary>
		/// See <see cref="Resources"/>
		/// </summary>
		private GameObject [] resources ;
		#endregion

		#region Constructor(s)
		/// <summary>
		/// Initializes a new instance of the <see cref="CodeMules.ObjectPooling.GameObjectPool"/> class.
		/// </summary>
		/// <param name="poolId">The unique pool identifier</param>
		/// <param name="prefab">The prefab template for pool resources</param>
		/// <param name="poolSize">The size of the pool</param>
		/// <param name="factory">The factory to allocate resources</param>
		/// <param name="resetter">(optional) method for resetting a resource when it is released</param>
		public GameObjectPool(
					string poolId,
					GameObject prefab,
					int poolSize = 16,
					Func<GameObjectPool,GameObject> factory = null,
					Func<GameObject,GameObject> resetter = null
				) : base(poolId,poolSize)
		{
			Prefab = prefab ;
			Factory = factory ?? DefaultResourceFactory;
			Resetter = resetter ;
		}
		#endregion
			
		#region Properties
		/// <summary>
		/// Get the factory used to allocate resources
		/// </summary>
		/// <value>The resoure factory</value>
		public Func<GameObjectPool,GameObject> Factory { get ; private set ; }

		/// <summary>
		/// Get the method used to reset a resource when it is released
		/// </summary>
		/// <value>The resetter method</value>
		public Func<GameObject,GameObject> Resetter { get ; private set ; }

		/// <summary>
		/// Get the prefab representation of the resource
		/// </summary>
		/// <value>The resource prefab</value>
		public GameObject Prefab { get ; private set ; }

		/// <summary>
		/// Get the <see cref="GameObject"/> that holds unallocated resources
		/// </summary>
		/// <value>A game object that holds unallocated resources</value>
		public GameObject ResourceGO { get ; internal set ; }
		#endregion

		#region Implementation
		/// <summary>
		/// Get/Set the next resource to be allocated w/o searching
		/// </summary>
		/// <value>The next available resource</value>
		private GameObject NextResource { get ; set; }

		/// <summary>
		/// Get the array that holds the resources
		/// </summary>
		/// <value>The items.</value>
		private GameObject [] Resources { 
			get {
				if (resources == null) {
					resources = new GameObject[Size-1] ;
				}

				return resources ;
			}
		}

		/// <summary>
		/// Allocate a resource
		/// </summary>
		/// <returns>The allocated resource</returns>
		protected internal GameObject AllocateResource() {
			GameObject result = Factory(this);

			PublishObservation(PoolObservationType.AllocateResource,result) ;

			return result;
		}

		/// <summary>
		/// Reset an item to its default state
		/// </summary>
		/// <returns>The resource</returns>
		/// <param name="go">The game object</param>
		private GameObject ResetResource(GameObject go) {
			// reparent to the game object that holds resources
			go.transform.SetParent(ResourceGO.transform) ;

			if (Resetter != null) {
				GameObject temp = go ;
				go = Resetter(go);

				if (!ReferenceEquals(go,temp)) {
					PublishObservation(
						ReferenceEquals(go,null) ? 
						PoolObservationType.ResetRevoked : 
						PoolObservationType.ResetReplaced,
						temp);
				}
			}

			return go ;
		}

		/// <summary>
		/// Pre allocate resources
		/// </summary>
		/// <param name="pool">The pool</param>
		/// <param name="preAllocate">The # of resources to pre allocate</param>
		internal void PreAllocate(GameObjectPool pool,int preAllocate) {
			if (preAllocate > 0) {
				NextResource = pool.AllocateResource();
			}

			for(int i=0; i < (preAllocate-1) ; i++) {
				if (Resources[i] == null) {
					Resources[i] = pool.AllocateResource();
				}
			}
		}

		/// <summary>
		/// Release a resource
		/// </summary>
		/// <param name="resource">The game object resource</param>
		internal void ReleaseResource(GameObject resource) {
			if (NextResource == null) {
				// fast release
				NextResource = ResetResource(resource) ;
				PublishObservation(PoolObservationType.ReleaseFast,resource);
			} else {
				// slow(er) release; grab 1st available slot
				for(int i=0; i < Resources.Length; i++) {
					if (Resources[i] == null) {
						Resources[i] = ResetResource(resource) ;
						PublishObservation(PoolObservationType.ReleaseSlow,resource);
						return ;
					}
				}

				// send prior to *destroy*
				PublishObservation(PoolObservationType.ReleaseDropped,resource);

				// pool is full up -- delete go now as it is not getting pooled
				UnityEngine.Object.DestroyImmediate(resource);
			}
		}
		#endregion

		#region Static Helpers
		/// <summary>
		/// Assign a pool identifier to a resource
		/// </summary>
		/// <returns>The pool identifier</returns>
		/// <param name="gameObject">The game object (resource) being pooled</param>
		/// <param name="poolId">the pool identifier</param>
		public static GameObject AssignPoolId(GameObject gameObject, string poolId) {
			GameObjectPoolId gopid = gameObject.GetComponent<GameObjectPoolId>() ;

			if (gopid == null) {
				gopid = gameObject.AddComponent<GameObjectPoolId>();
			}

			gopid.PoolId = poolId ;

			return gameObject;
		}

		/// <summary>
		/// The default resource allocation method
		/// </summary>
		/// <returns>a new resource</returns>
		/// <param name="pool">The pool that owns the resource</param>
		public static GameObject DefaultResourceFactory(GameObjectPool pool) {
			GameObject go = UnityEngine.Object.Instantiate(
				pool.Prefab,
				Vector3.zero,
				Quaternion.identity
			) as GameObject ;

			// parent the resource to the GO that "holds" all resources transform
			go.transform.SetParent(pool.ResourceGO.transform) ;

			return AssignPoolId(go,pool.Id);
		}
		#endregion
			
		#region IObjectPool<GameObject> imlementation
		/// <summary>
		/// Acquire a resource from the pool
		/// </summary>
		/// <remarks>
		/// <para>
		/// Concurrency is not required for Unity level object pools so we can
		/// make simplifying assumptions
		/// </para>
		/// </remarks>
		public override GameObject Acquire() {
			CheckClosed() ;

			// fast acquire
			GameObject result = NextResource ;

			if (result != null) {
				PublishObservation(PoolObservationType.AcquireFast,result);
				NextResource = null ;
			} else {
				// slow(er) acquire
				for(int i=0; i < Resources.Length ; i++) {
					if (Resources[i] != null) {
						result = Resources[i] ;
						Resources[i] = null ;
						PublishObservation(PoolObservationType.AcquireSlow,result);
						break ;
					}
				}
			}

			// if we couldnt acquire a resource we need to allocate one
			if (result == null) {
				result = AllocateResource() ;
				PublishObservation(PoolObservationType.AcquireAllocate,result) ;
			}

			return result ;
		}

		/// <summary>
		/// Acquire a resource from the pool as a component
		/// </summary>
		/// <typeparam name="TComponent">The component type to return</typeparam>
		public TComponent Acquire<TComponent>() {
			GameObject go = Acquire() ;
			return go.GetComponent<TComponent>();
		}

		/// <summary>
		/// Release a gameobject to the managing pool
		/// </summary>
		/// <param name="resource">The resource to be released to the pool</param>
		/// <exception cref="ObjectDisposedException">If the method is called after the pool has been disposed</exception>
		/// <remarks>
		/// The managing pool for the resource is verified by accessing the <see cref="GameObjectPoolId"/> component
		/// that was attached to <paramref name="resource"/> when it was allocated
		/// </remarks>
		public override void Release(GameObject resource) {
			CheckClosed() ;

			// Get the managing pool id so we can validate it
			GameObjectPoolId gopid = resource.GetComponent<GameObjectPoolId>() ;

			if (gopid == null) {
				LogHelpers.LogWarning("Attempt to release non-pooled GameObject");
				return ;
			}

			if (String.Compare(Id,gopid.PoolId,StringComparison.InvariantCultureIgnoreCase) != 0) {
				LogHelpers.LogError("Attempt to release GameObject from pool '{0}' to pool '{1}'",gopid.PoolId,Id);
				return ;
			}

			ReleaseResource(resource);
		}
		#endregion

		#region IDisposable Implementation
		/// <summary>
		/// Dispose the pool resources that are currently released
		/// </summary>
		/// <remarks>
		/// Only the non-acquired resources can be disposed. Any resources currently acquired
		/// will be destroyed when released (via <see cref="GameObjectPoolManager.Release(GameObject)"/>)
		/// </remarks>
		protected override void DisposeResources() {
			PublishObservation(PoolObservationType.Closed,null);

			if (ResourceGO != null) {
				// Destroy the parent => destroy the children
				UnityEngine.Object.DestroyImmediate(ResourceGO);

				ResourceGO = null ;
				NextResource = null ;
				resources = null ;
			}
		}
		#endregion
	}

}
