//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of 
//	this software and associated documentation files (the "Software"), to deal in 
//	the Software without restriction, including without limitation the rights to use, 
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//	Software, and to permit persons to whom the Software is furnished to do so, 
//	subject to the following conditions:
//  
//	The above copyright notice and this permission notice shall be included in all 
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

namespace CodeMules.ObjectPooling {
	/// <summary>
	/// Default implementation of an analytics observer for <seealso cref="IObjectPoolBase"/>
	/// </summary>
	/// <remarks>
	/// This class counts each pool observation and tracks the <seealso cref="PeakBalance"/> for
	/// resource demand. It does not auto-subscribe to an <seealso cref="IObjectPoolBase"/>, but
	/// can be attached to a pool using <seealso cref="AnalyticHandler"/>, or the owner of the
	/// instance can call <seealso cref="AnalyticHandler"/> directly
	/// </remarks>
	public class ObjectPoolAnalytics {
		private int [] rawStats ;
		
		/// <summary>
		/// Observation listener for an <seealso cref="IObjectPoolBase"/>
		/// </summary>
		/// <param name="pool">The pool being observed</param>
		/// <param name="observation">The observation</param>
		/// <param name="resource">The pool resource associated with the observation</param>
		public void AnalyticHandler(IObjectPoolBase pool,PoolObservationType observation,object resource) {
			Stats[(int) observation]++ ;

			switch(observation) {
				case PoolObservationType.AcquireFast:
				case PoolObservationType.AcquireSlow:
				case PoolObservationType.AcquireAllocate:
					if (++Balance > PeakBalance)
						PeakBalance = Balance ;
					break ;

				case PoolObservationType.ReleaseFast:
				case PoolObservationType.ReleaseSlow:
				case PoolObservationType.ReleaseDropped:
				case PoolObservationType.Forgotten:
					Balance-- ;
					break;

				case PoolObservationType.Closed:
					// although it appears to make sense to unsubscribe here, this class does not
					// actually own the subscription (because it didnt perform the subscribe) so 
					// removing the subscription here is inappropriate
					break;
			}
		}

		/// <summary>
		/// Clear the analytics data
		/// </summary>
		public void Clear() {
			if (rawStats == null)
				return ;

			Balance = PeakBalance = 0;
			
			for(int i=0; i < rawStats.Length; i++) {
				rawStats[i] = 0;
			}
		}

		/// <summary>
		/// Get the raw analytics data; an array indexed by <see cref="PoolObservationType"/>
		/// </summary>
		/// <value>The stats.</value>
		public int [] Stats {
			get {
				if (rawStats == null) {
					rawStats = new int[(int) PoolObservationType.LastObservation] ;
				}

				return rawStats ;
			}
		}

		/// <summary>
		/// Get the current balance of acquired resources
		/// </summary>
		/// <value>The resource balance</value>
		public int Balance { get; protected set ; }

		/// <summary>
		/// Get the peak balance of acquired resources
		/// </summary>
		/// <value>The peak balance</value>
		/// <remarks>
		/// Indicates the maximum demand for resources
		/// </remarks>
		public int PeakBalance { get ; protected set ; }

		/// <summary>
		/// Get the total number of resouces acquired
		/// </summary>
		/// <value>The total # of resources acquired</value>
		public int TotalAcquired {
			get {
				return Stats[(int) PoolObservationType.AcquireFast] +
					Stats[(int) PoolObservationType.AcquireSlow] +
					Stats[(int) PoolObservationType.AcquireAllocate] ;
			}
		}

		/// <summary>
		/// Get the total number of resource released
		/// </summary>
		/// <value>The total # of resources released</value>
		public int TotalReleased {
			get {
				return Stats[(int) PoolObservationType.ReleaseFast] +
					Stats[(int) PoolObservationType.ReleaseSlow] +
					Stats[(int) PoolObservationType.ReleaseDropped] ;
			}
		}

		/// <summary>
		/// Get the number of resources acquired in the most efficient manner
		/// </summary>
		/// <value>The number of resources acquired in the most efficient manner</value>
		public int AcquiredFast { 
			get {
				return Stats[(int) PoolObservationType.AcquireFast] ;
			}
		}

		/// <summary>
		/// Get the number of resources acquired in the less efficient manner
		/// </summary>
		/// <value>The number of resources acquired in the less efficient manner</value>
		public int AcquiredSlow { 
			get {
				return Stats[(int) PoolObservationType.AcquireSlow] ;
			}
		}

		/// <summary>
		/// Get the number of acquire requests that were satisfied by resource allocations
		/// </summary>
		/// <value>The number of acquire requests that were satisfied by resource allocations</value>
		public int AcquiredByAllocation { 
			get {
				return Stats[(int) PoolObservationType.AcquireAllocate] ;
			}
		}

		/// <summary>
		/// Get the number of resources released in the most efficient manner
		/// </summary>
		/// <value>The number of resources released in the most efficient manner</value>
		public int ReleasedFast { 
			get {
				return Stats[(int) PoolObservationType.ReleaseFast] ;
			}
		}

		/// <summary>
		/// Get the number of resources released in the less efficient manner
		/// </summary>
		/// <value>The number of resources released in the less efficient manner</value>
		public int ReleasedSlow { 
			get {
				return Stats[(int) PoolObservationType.ReleaseSlow] ;
			}
		}

		/// <summary>
		/// Get the number of resources that were dropped on release
		/// </summary>
		/// <value>The number of resources dropped on release</value>
		/// <remarks>
		/// A dropped resource occurs when a resource is released but it
		/// is not placed back into the pool (usually because the pool is full)
		/// </remarks>
		public int DroppedOnRelease { 
			get {
				return Stats[(int) PoolObservationType.ReleaseDropped] ;
			}
		}

		/// <summary>
		/// Get the number of resources that were revoked on release
		/// </summary>
		/// <value>The number of resources revoked on release</value>
		/// <remarks>
		/// A revoked resource occurs when a resource is released but a
		/// 'manager' decides it should not be re-added to the pool
		/// </remarks>
		public int RevokedOnReset {
			get {
				return Stats[(int) PoolObservationType.ResetRevoked] ;
			}
		}

		/// <summary>
		/// Get the number of resources that were replaced on release
		/// </summary>
		/// <value>The number of resources replaced on release</value>
		/// <remarks>
		/// A replaced resource occurs when a resource is released but a
		/// 'manager' decides it should replace that resource with a
		/// different resource
		/// </remarks>
		public int ReplacedOnReset {
			get {
				return Stats[(int) PoolObservationType.ResetReplaced] ;
			}
		}

		/// <summary>
		/// Get the number of resources that were forgotten
		/// </summary>
		/// <value>
		/// A consumer of a resource can ask that it be forgotten by the pool,
		/// thus allowing the consumer to retain it indefinitely
		/// </value>
		public int Forgotten {
			get {
				return Stats[(int) PoolObservationType.Forgotten] ;
			}
		}

		/// <summary>
		/// Get the number of resources were not forgotten because the resource
		/// was not known
		/// </summary>
		/// <value>
		/// A consumer of a resource can ask that it be forgotten by the pool,
		/// thus allowing the consumer to retain it indefinitely
		/// </value>
		public int ForgottenNotKnown {
			get {
				return Stats[(int) PoolObservationType.ForgetUnknown] ;
			}
		}
	}
}
