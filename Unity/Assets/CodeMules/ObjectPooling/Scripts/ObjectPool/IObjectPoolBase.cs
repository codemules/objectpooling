//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of 
//	this software and associated documentation files (the "Software"), to deal in 
//	the Software without restriction, including without limitation the rights to use, 
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//	Software, and to permit persons to whom the Software is furnished to do so, 
//	subject to the following conditions:
//  
//	The above copyright notice and this permission notice shall be included in all 
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------
using System ;

namespace CodeMules.ObjectPooling {
	/// <summary>
	/// Base functionality for object pools
	/// </summary>
	public interface IObjectPoolBase {
		/// <summary>
		/// A unique identifier for the pool
		/// </summary>
		/// <value>The unique pool identifier</value>
		string Id { get ; } 

		/// <summary>
		/// Observe pool actions
		/// </summary>
		/// <remarks>
		/// Delegate signature is essentially the following:
		/// <c>
		/// Hander(pool,PoolObservationType,resource)
		/// </c>
		/// <para>
		/// The parameters are "object" because most of the time, the specific type
		/// is not very important, and being type specific when it is not required
		/// can be annoying.
		/// </para>
		/// </remarks>
		event Action<IObjectPoolBase,PoolObservationType,object> Observe ;

		/// <summary>
		/// Determine if the <see cref="IObjectPool{T}"/> is providing resources
		/// </summary>
		/// <value>
		/// <c>true</c> if resources can be acquired/released, <c>false</c> otherwise
		/// </value>
		bool IsOpen { get ; }

		/// <summary>
		/// Determine if the <see cref="IObjectPool{T}"/> is unavailable (disposed)
		/// </summary>
		/// <value>
		/// <c>true</c> if the pool has been closed, <c>false</c> if 
		/// resources can be acquired/released
		/// </value>
		bool IsClosed { get ; }

		/// <summary>
		/// Get the target size of the pool
		/// </summary>
		/// <value>The target size of the pool</value>
		int Size { get ; }
	}
	
}
