//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of 
//	this software and associated documentation files (the "Software"), to deal in 
//	the Software without restriction, including without limitation the rights to use, 
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//	Software, and to permit persons to whom the Software is furnished to do so, 
//	subject to the following conditions:
//  
//	The above copyright notice and this permission notice shall be included in all 
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using System ;

namespace CodeMules.ObjectPooling {
	/// <summary>
	/// Interface for a general purpose object pool that can be used for
	/// POCOs (plain old CLR objects) and Unity Game Objects
	/// </summary>
	public interface IObjectPool<T> : IObjectPoolBase, IDisposable where T : class {
		/// <summary>
		/// Acquire a resource from the pool
		/// </summary>
		/// <remarks>
		/// This method should always succeed although it is allowed to block if resources are unavailable.
		/// It is allowed to throw in situations where an unexpected/runtime error occurs.
		/// <para>
		/// If an implementation deviates from the 'expected' behaviour it should be documented so that
		/// users of the implementation have some reasonable expectation of the altered behaviour
		/// </para>
		/// </remarks>
		T Acquire() ;

		/// <summary>
		/// Release a resource to the pool
		/// </summary>
		/// <param name="resource">The resource to be released to the pool</param>
		/// <remarks>
		/// The resource is not required to have been allocated by or previously managed by the
		/// pool when it is clear that the type is the same as the type managed by the pool. There
		/// are cases (Unity Game Objects) where type is not sufficient for determining compatibility,
		/// and in those cases the release may fail.
		/// <para>
		/// Implementations are allowed flexibility on dealing with possible error scenarios such as
		/// double releases, release of a resource not 'owned' by the pool, etc.
		/// </para>
		/// <para>
		/// Implementations should document their specific exceptional conditions
		/// </para>
		/// </remarks>
		void Release(T resource) ;

		/// <summary>
		/// Release a resource to the pool
		/// </summary>
		/// <param name="resource">The resource to be released to the pool</param>
		/// <remarks>
		/// Implementations are required to set <paramref name="resource"/> to <c>null</c> so that the callers
		/// reference is cleared and cannot be re-used. In the case of implementation specific exception
		/// condtions the implementation should use best judgement; preferring to clear <paramref name="resource"/>
		/// <para>
		/// Implementations are allowed flexibility on dealing with possible error scenarios such as
		/// double releases, release of a resource not 'owned' by the pool, etc.
		/// Implementations should document their specific exceptional conditions
		/// </para>
		/// </remarks>
		void ReleaseSafe(ref T resource) ;

		/// <summary>
		/// If the pool is tracking <paramref name="resource"/> in any way, the
		/// resource should be forgotten
		/// </summary>
		/// <param name="resource">The resource that should be forgotten</param>
		/// <remarks>
		/// This method is designed to allow an external entity that has acquired a resource
		/// to take control of it, and let the pool know that it should not be tracked in any way
		/// <para>
		/// It is implied that <paramref name="resource"/> has been acquired as the caller
		/// should not have a reference to it otherwise
		/// </para>
		/// </remarks>
		void Forget(T resource) ;
	}
}

