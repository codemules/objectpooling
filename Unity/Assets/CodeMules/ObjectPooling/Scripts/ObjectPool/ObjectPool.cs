//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of 
//	this software and associated documentation files (the "Software"), to deal in 
//	the Software without restriction, including without limitation the rights to use, 
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//	Software, and to permit persons to whom the Software is furnished to do so, 
//	subject to the following conditions:
//  
//	The above copyright notice and this permission notice shall be included in all 
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

//using UnityEngine;

using System ;
using System.Threading ;

using CodeMules.Utils;

namespace CodeMules.ObjectPooling {
	/// <summary>
	/// Default implementation of <see cref="IObjectPool{T}"/>
	/// </summary>
	/// <remarks>
	/// This implementation is mostly based on the following class from the Microsoft Roslyn C# Compiler Source:
	/// http://source.roslyn.codeplex.com/#Microsoft.CodeAnalysis/ObjectPool%601.cs
	/// <para>
	/// Specifically, the very non-rigid pooling philosophy, and the avoidance of using locks appealed to me. 
	/// I took quite a few liberties with this implementation; most of which I feel are reasonably good improvements 
	/// over the original.
	/// </para>
	/// <para>
	/// For this implementation:
	/// <list type="bullet">
	/// <item>
	/// <see cref="Acquire()"/> never blocks, and always returns a usable resource
	/// </item>
	/// <item>
	/// <see cref="Release"/> does not detect double releases and accepts resources not originally created by the pool
	/// </item>
	/// <item>
	/// <see cref="ObjectPoolBase{T}.ReleaseSafe"/> guarantees that the callers ref is set to <c>null</c>
	/// </item>
	/// <item>
	/// The pool never manages more than <c>poolSize</c> resources although it may allocate many more resources 
	/// then it manages
	/// </item>
	/// <item>
	/// Disposing the pool disposes the items in the pool (when applicable)
	/// </item>
	/// </list>
	/// </para>
	/// </remarks>
	public class ObjectPool<T> : ObjectPoolBase<T> where T : class {
		#region Fields
		/// <summary>
		/// Hold the resources for the pool items (except for <see cref="nextResource"/>)
		/// </summary>
		private T [] resources ;

		/// <summary>
		/// A cached resource, ready to return
		/// </summary>
		private T nextResource ;

		/// <summary>
		/// The factory method that creates resources for the pool when
		/// the resource being created wants a reference to the pool
		/// </summary>
		private readonly Func<IObjectPool<T>,T> factory ;

		/// <summary>
		/// The factory method that creates resources for the pool when
		/// the resource being created doesn't need a reference to the pool
		/// </summary>
		private readonly Func<T> simpleFactory ;

		/// <summary>
		/// Callback to "reset" a resource when it is released to the pool
		/// </summary>
		private readonly Func<T,T> resetter ;
		#endregion

		#region Constructor(s)
		/// <summary>
		/// Initializes a new instance of the <see cref="CodeMules.ObjectPooling.ObjectPool{T}"/> class.
		/// </summary>
		/// <param name="factory">The factory used to create resources for the pool</param>
		/// <param name="id">The unique identifier for the pool</param>
		/// <param name="resetter">(optional) method that "resets" an resource when it is released to the pool</param>
		/// <param name="poolSize">The maximum # of resources held by the pool</param>
		/// <param name="preAllocate">The # of resources to pre-allocate</param>
		public ObjectPool(
				string id,
				Func<IObjectPool<T>,T> factory, 
				Func<T,T> resetter = null,
				int poolSize = 16, 
				int preAllocate = 0
			) : base(id,poolSize)
		{
			Validation.CheckNotNull(factory);
			Validation.CheckNotNegative(preAllocate);

			this.factory = factory ;
			this.resetter = resetter ;

			Init(poolSize,Math.Min(poolSize,preAllocate));
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CodeMules.ObjectPooling.ObjectPool{T}"/> class.
		/// </summary>
		/// <param name="factory">The factory used to create resources for the pool</param>
		/// <param name="resetter">(optional) method that "resets" an resource when it is released to the pool</param>
		/// <param name="poolSize">The maximum # of resources held by the pool</param>
		/// <param name="preAllocate">The # of resources to pre-allocate</param>
		public ObjectPool(
				Func<IObjectPool<T>,T> factory, 
				Func<T,T> resetter = null,
				int poolSize = 16, 
				int preAllocate = 0
			) : this(Guid.NewGuid().ToString(),factory,resetter,poolSize,preAllocate) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="CodeMules.ObjectPooling.ObjectPool{T}"/> class.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="factory">The factory used to create resources for the pool</param>
		/// <param name="resetter">(optional) method that "resets" an resource when it is released to the pool</param>
		/// <param name="poolSize">The maximum # of resources held by the pool</param>
		/// <param name="preAllocate">The # of resources to pre-allocate</param>
		public ObjectPool(
				string id,
				Func<T> factory, 
				Func<T,T> resetter = null,
				int poolSize = 16, 
				int preAllocate = 0
				) : base(id,poolSize)
		{
			Validation.CheckNotNull(factory);
			Validation.CheckNotNegative(preAllocate);

			simpleFactory = factory ;
			this.resetter = resetter ;

			Init(poolSize,Math.Min(poolSize,preAllocate));
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CodeMules.ObjectPooling.ObjectPool{T}"/> class.
		/// </summary>
		/// <param name="factory">The factory used to create resources for the pool</param>
		/// <param name="resetter">Method that "resets" an resource when it is released to the pool</param>
		/// <param name="poolSize">The maximum # of resources held by the pool</param>
		/// <param name="preAllocate">The # of resources to pre-allocate</param>
		public ObjectPool(
				Func<T> factory, 
				Func<T,T> resetter = null,
				int poolSize = 16, 
				int preAllocate = 0
			) : this(Guid.NewGuid().ToString(),factory,resetter,poolSize,preAllocate) {}
		#endregion

		#region IObjectPool implementation
		/// <summary>
		/// Acquire a resource from the pool
		/// </summary>
		/// <exception cref="ObjectDisposedException">If the method is called after the pool has been disposed</exception>
		public override T Acquire() {
            CheckClosed() ;

			T resource = nextResource ;

			// Assume AquireFast
			if (ReferenceEquals(resource,null) || !ReferenceEquals(resource,Interlocked.CompareExchange(ref nextResource,default(T),resource))) { 
				resource = AcquireFromPool();
			} else {
				PublishObservation(PoolObservationType.AcquireFast,resource);
			}

			return resource ;
		}

		/// <summary>
		/// Release a resource to the pool
		/// </summary>
		/// <param name="resource">The resource to be released to the pool</param>
		/// <exception cref="ObjectDisposedException">If the method is called after the pool has been disposed</exception>
		public override void Release(T resource) {
			Validation.CheckNotNull(resource) ;

            CheckClosed() ;

			if (ReferenceEquals(nextResource,null)) {
				PublishObservation(PoolObservationType.ReleaseFast,resource);
				nextResource = ResetResource(resource) ;
			} else {
				ReleaseToPool(resource);
			}
		}
		#endregion

		#region Implementation
		/// <summary>
		/// Initialze the pool
		/// </summary>
		/// <param name="poolSize">The maximum # of resources held by the pool</param>
		/// <param name="preAllocate">The # of resources to pre-allocate</param>
		private void Init(int poolSize,int preAllocate) {
			resources = new T[poolSize-1] ;

			if (preAllocate > 0) {
				PreAllocateResources(preAllocate);
			}
		}

		/// <summary>
		/// Pre-allocate resources
		/// </summary>
		/// <param name="count">The number of resources to allocate</param>
		private void PreAllocateResources(int count) {
			if (count > 0) {
				nextResource = AllocateResource();
			}

			for(int i=0; i < (count-1); i++) {
				resources[i] = AllocateResource();
			}
		}

		/// <summary>
		/// Create a pool resource 
		/// </summary>
		/// <returns>The resource</returns>
		private T AllocateResource() {
			T result = (factory != null) ? factory(this) : simpleFactory();
			PublishObservation(PoolObservationType.AllocateResource,result);
			return result;
		}

		/// <summary>
		/// Reset the resource to a "new" state so it can be reused (if a resetter was provided)
		/// </summary>
		/// <returns>The resource</returns>
		/// <param name="resource">The resource to be reset</param>
		private T ResetResource(T resource) {
			if (resetter != null) {
				T temp = resource ;
				resource = resetter(resource);

				if (!ReferenceEquals(resource,temp)) {
					PublishObservation(
						ReferenceEquals(resource,null) ? 
							PoolObservationType.ResetRevoked : 
							PoolObservationType.ResetReplaced,
						temp);
					}
			}

			return resource;
		}

		/// <summary>
		/// Find a resource within the pool, or allocate a new resource
		/// </summary>
		/// <returns>The from pool.</returns>
		/// <remarks>
		/// This method uses a linear search for the first available resource which
		/// helps with performance as <see cref="ReleaseToPool(T)"/> stores free 
		/// resources as close to the front of the list as possible.
		/// </remarks>
		private T AcquireFromPool() {
			for(int i=0; i < resources.Length; i++) {
				T resource = resources[i] ;

				// AcquireSlow
				if (!ReferenceEquals(resource,null) && ReferenceEquals(resource,Interlocked.CompareExchange(ref resources[i],null,resource))) {
					PublishObservation(PoolObservationType.AcquireSlow,resource);
					return resource ;
				}
			}

			T newResource = AllocateResource();

			PublishObservation(PoolObservationType.AcquireAllocate,newResource);

			return newResource ;
		}

		/// <summary>
		/// Release a resource to the pool
		/// </summary>
		/// <param name="resource">The resource</param>
		/// <remarks>
		/// This method uses a linear search for an empty slot which results in free resources
		/// being placed as near to the front of the list as possible. This helps the performance
		/// of <see cref="Acquire"/> which searches for availabe resources in the same manner.
		/// <para>
		/// Note that if all the pool slots are in use then <paramref name="resource"/> is dropped and left
		/// for normal garbage collection
		/// </para>
		/// </remarks>
		private void ReleaseToPool(T resource) {
			for(int i=0; i < resources.Length; i++) {
				//	------------------------------------------------------------------------
				//	Without sycnhronization and with concurrent access, this block could
				//	result in two values being stored in the same slot. This should be 
				//	infrequent, and ultimately is harmless as one of the resources will
				//	end up subjected to normal garbage collection
				//	------------------------------------------------------------------------
				
				if (ReferenceEquals(resources[i],null)) {
					resources[i] = ResetResource(resource) ;
					PublishObservation(PoolObservationType.ReleaseSlow, resource);
					return ;
				}
			}

			PublishObservation(PoolObservationType.ReleaseDropped,resource);

			// resource subject to normal GC
		}
		#endregion

		#region IDisposable implementation
		/// <summary>
		/// Dispose the pool, and any resources that implement <see cref="IDisposable"/>
		/// </summary>
		protected override void DisposeResources() {
			PublishObservation(PoolObservationType.Closed,null);

			try { 
				IDisposable resource = nextResource as IDisposable ;
				if (!ReferenceEquals(resource,null)) {
					resource.Dispose() ; 
				}
				nextResource = null ;
			} catch {}

			for(int i=0; i < resources.Length; i++) {
				IDisposable resource = resources[i] as IDisposable ;
				if (!ReferenceEquals(resource,null)) {
					(resources[i] as IDisposable).Dispose() ;
				}
			}
			resources = null ;
		}
		#endregion
	}
}
