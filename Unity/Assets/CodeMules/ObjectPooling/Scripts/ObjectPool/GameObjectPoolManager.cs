﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of 
//	this software and associated documentation files (the "Software"), to deal in 
//	the Software without restriction, including without limitation the rights to use, 
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//	Software, and to permit persons to whom the Software is furnished to do so, 
//	subject to the following conditions:
//  
//	The above copyright notice and this permission notice shall be included in all 
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using UnityEngine;

using System ;
using System.Collections.Generic ;
using CodeMules.Utils;

namespace CodeMules.ObjectPooling {
	/// <summary>
	/// Game object pool manager - manages pools of <see cref="GameObject"/> instances
	/// </summary>
	public class GameObjectPoolManager : MonoBehaviour {
		#region Inspector Fields
		/// <summary>
		/// Determine if the pool manager should exist across scenes (<c>true</c>) or
		/// have its lifetime bound to the lifetime of the scene (<c>false</c>)
		/// </summary>
		[Tooltip("True to persist the pool manager across scenes, false to desotry it when the scene unloads")]
		public bool PersistAcrossScenes ;
		#endregion

		#region Fields
		private IDictionary<string,GameObjectPool> poolIdToPool ;
		#endregion

		#region Unity Methods
		void Awake() {
			if (PersistAcrossScenes) {
				// Note that this will only work if our gameObject is top-level
				UnityEngine.Object.DontDestroyOnLoad(gameObject) ;
			}
		}

		void OnDestroy() {
			CloseAllPools();
		}
		#endregion

		#region Methods
		/// <summary>
		/// Add a pool to be managed
		/// </summary>
		/// <returns>The object pool added</returns>
		/// <param name="poolId">The pool identifier</param>
		/// <param name="poolPrefab">The prefab used to allocate resources</param>
		/// <param name="poolSize">The size of the pool</param>
		/// <param name="preAllocate">The # of items to pre allocate</param>
		/// <param name="factory">The (optional) factory method to create resources</param>
		/// <param name="resetter">The (optional) method for resetting resources when they are released</param>
		public IObjectPool<GameObject> AddPool(
					string poolId,
					GameObject poolPrefab, 
					int poolSize = 8,
					int preAllocate = 0,
					Func<GameObjectPool,GameObject> factory = null,
					Func<GameObject,GameObject> resetter = null
					) 
		{
				return AddPool(
					new GameObjectPool(
						poolId,
						poolPrefab,
						poolSize,
						factory,
						resetter
					),
					preAllocate
				);
		}

		/// <summary>
		/// Add a pool to be managed
		/// </summary>
		/// <returns>The object pool added</returns>
		/// <param name="pool">the pool to add</param>
		/// <param name="preAllocate">The # of items to pre allocate</param>
		public IObjectPool<GameObject> AddPool(GameObjectPool pool,int preAllocate = 0) {
			if (GetPoolInternal(pool.Id) != null) {
				LogHelpers.LogError("Cannot add pool '{0}' because it already exists!",pool.Id);
				return null;
			}

			// Create a parent GO to hold all the free resources
			pool.ResourceGO = CreatePoolGO(pool.Id) ;

			PoolIdToPool.Add(pool.Id,pool);

			if (preAllocate > 0) {
				pool.PreAllocate(pool,Math.Min(pool.Size,preAllocate)) ;
			}

			return pool ;
		}

		/// <summary>
		/// Get an object pool by identifier
		/// </summary>
		/// <returns>The pool</returns>
		/// <param name="poolId">the pool identifier</param>
		public IObjectPool<GameObject> GetPool(string poolId) { 	
			return GetPoolInternal(poolId);
		}

		/// <summary>
		/// Calose all pools being managed
		/// </summary>
		public void CloseAllPools() {
			foreach(var pool in PoolIdToPool.Values) {
				pool.Dispose();
			}

			PoolIdToPool.Clear();
		}

		/// <summary>
		/// Close the pool with the specified identifier
		/// </summary>
		/// <param name="poolId">The pool identifier</param>
		public void ClosePool(string poolId) {
			GameObjectPool pool = GetPoolInternal(poolId) ;

			// The pool for the item no longer exists so destroy the item
			if (pool == null) {
				LogHelpers.LogWarning("Attempt to release non-pooled GameObject");
				return ;
			}

			// remove our reference
			PoolIdToPool.Remove(poolId);

			// dispose the pool and its items
			pool.Dispose();
		}

		/// <summary>
		/// Acquire a resource from the pool with a specified identifier
		/// </summary>
		/// <param name="poolId">the pool identifier</param>
		public GameObject Acquire(string poolId) {
			GameObjectPool pool = GetPoolInternal(poolId) ;

			// The pool doesnt exist . . . cant return null so we have to throw
			if (pool == null) {
				Exception ex = new ArgumentException("Attempt to acquire resource for unknown or closed pool","poolId");
				LogHelpers.LogError(ex);
				throw ex ;
			}

			return pool.Acquire();
		}

		/// <summary>
		/// Acquire a resource from the pool with a specified identifier
		/// </summary>
		/// <param name="poolId">the pool identifier</param>
		/// <typeparam name="T">The component type to acquire</typeparam>
		public T Acquire<T>(string poolId) {
			GameObject go = Acquire(poolId) ;
			return go.GetComponent<T>();
		}
			
		/// <summary>
		/// Release a gameobject to the managing pool
		/// </summary>
		/// <param name="resource">The resource to be released to the pool</param>
		/// <exception cref="ObjectDisposedException">If the method is called after the pool has been disposed</exception>
		/// <remarks>
		/// The managing pool is determined by accessing the <see cref="GameObjectPoolId"/> component
		/// that was attached to <paramref name="resource"/> when it was allocated
		/// </remarks>
		public void Release(GameObject resource) {
			GameObjectPoolId gopid = resource.GetComponent<GameObjectPoolId>() ;

			if (gopid == null) {
				LogHelpers.LogWarning("Attempt to release non-pooled GameObject");
				return ;
			}

			// find the managing pool
			GameObjectPool pool = GetPoolInternal(gopid.PoolId) ;

			if (pool != null) {
				pool.ReleaseResource(resource);
			} else {
				// The pool for the item no longer exists so destroy the item
				DestroyImmediate(resource);
			}
		}
		#endregion Methods

		#region Helpers
		private GameObject CreatePoolGO(string poolId) {
			GameObject result = new GameObject(poolId);
			result.SetActive(false);
			result.name = string.Format("{0} - Pool Items",poolId);

			result.transform.SetParent(gameObject.transform,false);
			result.transform.localRotation = Quaternion.identity;
			result.transform.localPosition = Vector3.zero ;
			result.transform.localScale = Vector3.one ;

			return result ;
		}

		private GameObjectPool GetPoolInternal(string poolId) {
			GameObjectPool pool ;

			if (PoolIdToPool.TryGetValue(poolId,out pool)) {
				// if the pool got closed, remove it . . .
				if (pool.IsClosed) {
					PoolIdToPool.Remove(poolId);
					pool = null ;
				}
			}

			return pool ;
		}
			
		private IDictionary<string,GameObjectPool> PoolIdToPool {
			get {
				if (poolIdToPool == null) {
					poolIdToPool = new Dictionary<string, GameObjectPool>(StringComparer.InvariantCultureIgnoreCase) ;
				}

				return poolIdToPool ;
			}
		}
		#endregion
	}
}
