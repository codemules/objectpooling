﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of 
//	this software and associated documentation files (the "Software"), to deal in 
//	the Software without restriction, including without limitation the rights to use, 
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//	Software, and to permit persons to whom the Software is furnished to do so, 
//	subject to the following conditions:
//  
//	The above copyright notice and this permission notice shall be included in all 
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

using UnityEngine;
using CodeMules.Utils ;

namespace CodeMules.ObjectPooling {
	/// <summary>
	/// A pool creation script
	/// </summary>
	/// <remarks>
	/// Note that this script is responsible for creating and adding the pool to
	/// a <see cref="GameObjectPoolManager"/> but is not involved in other lifetime
	/// events
	/// </remarks>
	public class PoolCreator : MonoBehaviour {
		/// <summary>
		/// The <see cref="GameObjectPoolManager"/> that manages the pool created by this 
		/// script instance
		/// </summary>
		[Tooltip("The pool manager that manages the pool created by this script instance")]
		public GameObjectPoolManager PoolManager ;

		/// <summary>
		/// The unique pool identifier
		/// </summary>
		[Tooltip("The unique pool identifier")]
		public string PoolId ;

		/// <summary>
		/// The prefab used to allocate resources
		/// </summary>
		[Tooltip("The prefab used to allocate resources")]
		public GameObject PoolPrefab ;

		/// <summary>
		/// The maximum # of resources managed by the pool
		/// </summary>
		[Tooltip("The maximum # of resources managed by the pool")]
		public int PoolSize = 8 ;

		/// <summary>
		/// The # of resources to pre-allocate
		/// </summary>
		[Tooltip("The # of resources to pre-allocate")]
		public int PreAllocate ;

		#region Unity Callbacks
		/// <summary>
		/// Create/Add the pool on start
		/// </summary>
		void Start () {
			AddPoolSafely();
		}
		#endregion

		/// <summary>
		/// Create/Add the pool to the pool manager if (and only if) all the appropriate
		/// values are valid
		/// </summary>
		protected void AddPoolSafely() {
			if (string.IsNullOrEmpty(PoolId) || (PoolPrefab == null)) {
				LogHelpers.LogWarning("Cannot create {0} unless PoolId and PoolPrefab are specified",typeof(GameObjectPool).Name);
			}

			if (PoolManager == null) {
				LogHelpers.LogWarning("Cannot create Pool '{0}' because a reference for {1} is not specified",PoolId,typeof(GameObjectPoolManager).Name);
			}

			AddPool();
		}

		/// <summary>
		/// Create/Add the pool to the pool manager
		/// </summary>
		protected virtual void AddPool() {
			PoolManager.AddPool(
				PoolId,
				PoolPrefab,
				PoolSize,
				Mathf.Min(PreAllocate,PoolSize)
			) ;
		}
	}
}
