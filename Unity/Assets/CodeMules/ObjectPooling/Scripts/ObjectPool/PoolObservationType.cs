//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of 
//	this software and associated documentation files (the "Software"), to deal in 
//	the Software without restriction, including without limitation the rights to use, 
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//	Software, and to permit persons to whom the Software is furnished to do so, 
//	subject to the following conditions:
//  
//	The above copyright notice and this permission notice shall be included in all 
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

namespace CodeMules.ObjectPooling {

	/// <summary>
	/// Enumeration of operations that can be observed on an <c>ObjectPool</c>
	/// </summary>
	public enum PoolObservationType {
		/// <summary>
		/// A resource was allocated
		/// </summary>
		AllocateResource = 0,

		/// <summary>
		/// A resource was acquired via an optimized path
		/// </summary>
		AcquireFast,
		/// <summary>
		/// A resource was acquired via an unoptimized path
		/// </summary>
		AcquireSlow,
		/// <summary>
		/// A resource was acquired by allocation
		/// </summary>
		AcquireAllocate,

		/// <summary>
		/// A resource was released via an optimized path
		/// </summary>
		ReleaseFast,
		/// <summary>
		/// A resource was released via an unoptimized path
		/// </summary>
		ReleaseSlow,
		/// <summary>
		/// A resource was dropped/released because the pool is full
		/// </summary>
		ReleaseDropped,

		/// <summary>
		/// A resource was revoked during a reset operation
		/// </summary>
		ResetRevoked,
		/// <summary>
		/// A resource was replaced with a different resource during a reset operation
		/// </summary>
		ResetReplaced,

		/// <summary>
		/// Something was forgotten
		/// </summary>
		Forgotten,
		/// <summary>
		/// The forget request referred to a resource that was not known
		/// </summary>
		ForgetUnknown,

		/// <summary>
		/// The pool is closed
		/// </summary>
		Closed,

		/// <summary>
		/// A marker so that implementations can create array's of an appropriate size
		/// for keeping observation counters 
		/// </summary>
		LastObservation
	}
	
}
