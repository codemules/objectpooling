//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of 
//	this software and associated documentation files (the "Software"), to deal in 
//	the Software without restriction, including without limitation the rights to use, 
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//	Software, and to permit persons to whom the Software is furnished to do so, 
//	subject to the following conditions:
//  
//	The above copyright notice and this permission notice shall be included in all 
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

#if UNITY_5_3 || UNITY_5_3_OR_NEWER
using System;

using NUnit.Framework ;

using CodeMules.ObjectPooling ;

namespace ObjectPool.Tests {
	[TestFixture]
	public class TestsOfPoolWrapper {
		[TestFixtureSetUp] 
		[TearDown]
		public void ClearStats() {
			PoolTestItem.Clear();
		}

		[Test]
		public void TestConstruction() {
			IObjectPool<PoolWrapper<PoolTestItem>> pool = PoolWrapper<PoolTestItem>.CreatePool(16);
			Assert.AreEqual(0,PoolTestItem.CreateCount) ;
			Assert.IsTrue(pool.IsOpen) ;
			Assert.IsFalse(pool.IsClosed) ;

			pool = PoolWrapper<PoolTestItem>.CreatePool(16,4);
			Assert.AreEqual(4,PoolTestItem.CreateCount) ;
			Assert.IsTrue(pool.IsOpen) ;
			Assert.IsFalse(pool.IsClosed) ;
		}

		[Test]
		public void TestAcquire() {
			IObjectPool<PoolWrapper<PoolTestItem>> pool = PoolWrapper<PoolTestItem>.CreatePool(16);
			// establish nothing has been pre-allocated
			Assert.AreEqual(0,PoolTestItem.CreateCount) ;

			// normal usage pattern
			using(var ptiw = pool.Acquire()) {
				// pool needed to create an instance
				Assert.AreEqual(1,PoolTestItem.CreateCount) ;

				Assert.IsNotNull(ptiw.Resource);
				Assert.IsNotNull((PoolTestItem) ptiw);
			}

			// released back to pool (not really disposed)
			Assert.AreEqual(0,PoolTestItem.DisposeCount);

			// Not supposed to do this; allows us to test disposal protocol
			var ptiw2 = pool.Acquire() ;
			// still only one instance created (same instance was released back to the pool)
			Assert.AreEqual(1,PoolTestItem.CreateCount) ;

			// bye-bye pool
			pool.Dispose();
			// pool didnt have any instances to dispose
			Assert.AreEqual(0,PoolTestItem.DisposeCount);

			// Pool is disposed so this should be a *real* dispose
			ptiw2.Dispose() ;
			try {
				// This should throw since we are being naughty and referencing a disposed item
				if (ptiw2.Resource != null) {} ;
				Assert.Fail("Expecting " + typeof(ObjectDisposedException));
			} catch(ObjectDisposedException) {
			}

			// our 1 instance should have been disposed
			Assert.AreEqual(1,PoolTestItem.DisposeCount);
		}

		[Test]
		public void TestDispose() {
			IObjectPool<PoolWrapper<PoolTestItem>> pool = PoolWrapper<PoolTestItem>.CreatePool(16);
			// establish nothing has been pre-allocated
			Assert.AreEqual(0,PoolTestItem.CreateCount) ;

			using(var ptiw = pool.Acquire()) {
				// pool needed to create an instance
				Assert.AreEqual(1,PoolTestItem.CreateCount) ;
			} // here a *release* of ptiw occurs

			// released back to pool (not really disposed)
			Assert.AreEqual(0,PoolTestItem.DisposeCount);

			using(var ptiw = pool.Acquire()) {
				// pool did NOT need to create an instance (we released an instance back)
				Assert.AreEqual(1,PoolTestItem.CreateCount) ;
			} // here a *release* of ptiw occurs

			// released back to pool (not really disposed)
			Assert.AreEqual(0,PoolTestItem.DisposeCount);

			using(var ptiw = pool.Acquire()) {
				// pool did NOT need to create an instance (we released an instance back)
				Assert.AreEqual(1,PoolTestItem.CreateCount) ;

				// bye-bye pool
				pool.Dispose();
				// pool didnt have any instances to dispose
				Assert.AreEqual(0,PoolTestItem.DisposeCount);
			} // Here a *real* dispose of ptiw occurs

			// our 1 instance should have been disposed
			Assert.AreEqual(1,PoolTestItem.DisposeCount);
		}
	}
}
#endif		// UNITY_5_3 || UNITY_5_3_OR_NEWER
