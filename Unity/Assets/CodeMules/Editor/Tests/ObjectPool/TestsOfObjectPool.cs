﻿//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of 
//	this software and associated documentation files (the "Software"), to deal in 
//	the Software without restriction, including without limitation the rights to use, 
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//	Software, and to permit persons to whom the Software is furnished to do so, 
//	subject to the following conditions:
//  
//	The above copyright notice and this permission notice shall be included in all 
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

#if UNITY_5_3 || UNITY_5_3_OR_NEWER
using NUnit.Framework ;

using CodeMules.ObjectPooling ;

namespace ObjectPool.Tests {
	[TestFixture]
	public class TestsOfObjectPool {
		[TestFixtureSetUp] 
		[TearDown]
		public void ClearStats() {
			PoolTestItem.Clear();
		}

		[Test]
		public void TestConstruction() {
			IObjectPool<PoolTestItem> pool = new ObjectPool<PoolTestItem>(PoolTestItem.Create);
			Assert.AreEqual(0,PoolTestItem.CreateCount) ;
			Assert.IsTrue(pool.IsOpen) ;
			Assert.IsFalse(pool.IsClosed) ;

			pool = new ObjectPool<PoolTestItem>(PoolTestItem.Create,null,16,4);
			Assert.AreEqual(4,PoolTestItem.CreateCount) ;
			Assert.IsTrue(pool.IsOpen) ;
			Assert.IsFalse(pool.IsClosed) ;

			PoolTestItem.CreateCount = 0 ;

			pool = new ObjectPool<PoolTestItem>(PoolTestItem.SimpleCreate,null,16,4);
			Assert.AreEqual(4,PoolTestItem.CreateCount) ;
			Assert.IsTrue(pool.IsOpen) ;
			Assert.IsFalse(pool.IsClosed) ;
		}

		[Test]
		public void TestDispose() {
			IObjectPool<PoolTestItem> pool = new ObjectPool<PoolTestItem>(PoolTestItem.Create,null,16,4);

			pool.Dispose() ;
			Assert.IsTrue(pool.IsClosed) ;
			Assert.IsFalse(pool.IsOpen);
			Assert.AreEqual(4,PoolTestItem.DisposeCount);
		}

		[Test]
		public void TestAcquire() {
			IObjectPool<PoolTestItem> pool = new ObjectPool<PoolTestItem>(PoolTestItem.Create,null,16,4);
			Assert.AreEqual(4,PoolTestItem.CreateCount) ;

			DoAcquire(pool) ;
			Assert.AreEqual(4,PoolTestItem.CreateCount) ;

			DoAcquire(pool) ;
			Assert.AreEqual(4,PoolTestItem.CreateCount) ;

			DoAcquire(pool) ;
			Assert.AreEqual(4,PoolTestItem.CreateCount) ;

			DoAcquire(pool) ;
			Assert.AreEqual(4,PoolTestItem.CreateCount) ;

			DoAcquire(pool) ;
			Assert.AreEqual(5,PoolTestItem.CreateCount) ;	
		}

		[Test]
		public void TestAcquireNewInstance() {
			const int poolSize = 8 ;

			IObjectPool<PoolTestItem> pool = new ObjectPool<PoolTestItem>(PoolTestItem.Create,null,poolSize);
			Assert.AreEqual(0,PoolTestItem.CreateCount) ;

			for(int i=0; i < (2*poolSize) ; i++) {
				DoAcquire(pool);
			}

			Assert.AreEqual(2*poolSize,PoolTestItem.CreateCount);
		}

		[Test]
		public void TestReleaseSafe() {
			IObjectPool<PoolTestItem> pool = new ObjectPool<PoolTestItem>(PoolTestItem.SimpleCreate);

			PoolTestItem pti1 = DoAcquire(pool) ;
			Assert.AreEqual(1,PoolTestItem.CreateCount) ;

			pool.ReleaseSafe(ref pti1) ;
			Assert.IsNull(pti1);

			pti1 = DoAcquire(pool) ;
			Assert.AreEqual(1,PoolTestItem.CreateCount) ;
		}

		[Test]
		public void TestRelease() {
			int itemsReset = 0 ;

			IObjectPool<PoolTestItem> pool = new ObjectPool<PoolTestItem>(
				PoolTestItem.SimpleCreate,
				(item) => {
					itemsReset++;
					return item ;
				}
			);

			PoolTestItem pti1 = DoAcquire(pool) ;
			Assert.AreEqual(1,PoolTestItem.CreateCount) ;

			pool.ReleaseSafe(ref pti1) ;
			Assert.IsNull(pti1);
			Assert.AreEqual(1,itemsReset);

			pti1 = DoAcquire(pool) ;
			Assert.AreEqual(1,PoolTestItem.CreateCount) ;
			pool.Release(pti1) ;
			Assert.AreEqual(2,itemsReset);
		}
			
		private PoolTestItem DoAcquire(IObjectPool<PoolTestItem> pool) {
			PoolTestItem pti = pool.Acquire();
			Assert.IsNotNull(pti) ;
			return pti ;
		}
	}

}
#endif		// UNITY_5_3 || UNITY_5_3_OR_NEWER
