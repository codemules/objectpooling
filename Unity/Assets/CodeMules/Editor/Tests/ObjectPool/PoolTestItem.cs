//	--------------------------------------------------------------------------------
//	Copyright (C) 2016, Code Mules - All Rights Reserved.
//
//	Permission is hereby granted, free of charge, to any person obtaining a copy of 
//	this software and associated documentation files (the "Software"), to deal in 
//	the Software without restriction, including without limitation the rights to use, 
//	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
//	Software, and to permit persons to whom the Software is furnished to do so, 
//	subject to the following conditions:
//  
//	The above copyright notice and this permission notice shall be included in all 
//	copies or substantial portions of the Software.
//
//	The Original Code and all software distributed under the License are
//	distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
//	EXPRESS OR IMPLIED, AND CODE MULES HEREBY DISCLAIMS ALL SUCH WARRANTIES,
//	INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
//	FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
//	--------------------------------------------------------------------------------

#if UNITY_5_3 || UNITY_5_3_OR_NEWER

using System;

using CodeMules.ObjectPooling ;

namespace ObjectPool.Tests {
	class PoolTestItem : IDisposable {
		public static int CreateCount { get ; set  ; }
		public static int DisposeCount { get ; set  ; }

		public static void Clear() {
			CreateCount = DisposeCount = 0 ;
		}

		public static PoolTestItem Create(IObjectPool<PoolTestItem> pool) {
			return new PoolTestItem();
		}

		public static PoolTestItem SimpleCreate() {
			return new PoolTestItem();
		}

		public PoolTestItem() {
			CreateCount++ ;
		}

		#region IDisposable implementation
		protected virtual void Dispose(bool isDirectDispose) {
			if (isDirectDispose) {
				DisposeCount++ ;
			}
		}

		public void Dispose() {
			Dispose(true) ;
			GC.SuppressFinalize(this);
		}

		~PoolTestItem() {
			Dispose(false);
		}
		#endregion
	}
}

#endif		// UNITY_5_3 || UNITY_5_3_OR_NEWER
